package br.com.moolab.tagbier.utils;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;

public class CameraUtil {

	public static boolean isIntentAvailable(Context context, String action) {
		final PackageManager packageManager = context.getPackageManager();
		final Intent intent = new Intent(action);
		List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

	public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth, int bitmapHeight) {
		return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
	}

	public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
		int width = image.getWidth();
		int height = image.getHeight();
						
		float bitmapRatio = width / height;
		if (bitmapRatio > 0) {
			width = maxSize;
			height = (int) (width / bitmapRatio);
		} else {
			height = maxSize;
			width = (int) (height * bitmapRatio);
		}
		
		if (width <= 0 || height <= 0) {
			return image;
		}
				
		return Bitmap.createScaledBitmap(image, width, height, true);
	}
}
