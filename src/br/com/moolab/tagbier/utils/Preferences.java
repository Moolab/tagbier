package br.com.moolab.tagbier.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Preferences {

	public static final String PREF_NAME = "meBierPreferences";
	public static final String TOTAL_USER = "totalUser";
	
	public static int getTotalUser(Context context) {	
		SharedPreferences pref = context.getApplicationContext().getSharedPreferences(Preferences.PREF_NAME, 0);
		return pref.getInt(Preferences.TOTAL_USER, 1);
	}

	public static void saveTotalUser(Context context, int total) {		
				
		SharedPreferences pref = context.getApplicationContext().getSharedPreferences(Preferences.PREF_NAME, 0);
		Editor editor = pref.edit();
		editor.putInt(Preferences.TOTAL_USER, total);
		editor.commit();
	}
}
