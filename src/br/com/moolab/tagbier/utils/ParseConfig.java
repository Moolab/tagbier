package br.com.moolab.tagbier.utils;

public final class ParseConfig {

	// Producao
	// public static String appID = "hWG1U1985LxSpP4C4dxj0kLe7E3Lx8vfPWSOxQTF";
	// public static String clientID =
	// "8b4crd0MznpSp4ncKWTO3Ttd4ZF8VhjzV93choRR";

	// Homologacao
	public static String appID = "rpx72T7rw2as0SqQVmDOsYmTrsr1pu50PynX0x1Z";
	public static String clientID = "7rTQGs74dt7QViSic4stdzgyKidyE5eDLfEk6rkK";
	

	public static final String UPDATEDAT = "updatedAt";
	public static final String OBJCETID = "objectID";

	public static final String PULL_TYPE_ALL = "PULL_TYPE_ALL";
	public static final String PULL_TYPE_FEW = "PULL_TYPE_FEW";
	public static final String PULL_TYPE_FIRST = "PULL_TYPE_FIRST";

	public static final String TAG_ERROR = "ERRO_PARSE";
	public static final String TAG_SUCCESS = "SUCESSO PARSE";

	public static final String OBJECT_BEER = "Beer";
	public static final String OBJECT_HISTORY = "History";
	
	public static final String BEER_TITULO = "titulo";
	public static final String BEER_TIPO = "tipo";
	public static final String BEER_FOTO = "foto";
	public static final String BEER_HASHTAGS = "hashtags";
	public static final String BEER_QUANDO = "quando";	

	public static final String POINTER_USER = "user";
	public static final String BEER_PHOTO_ID = "photoID";
	public static final String BEER_POST_ID = "postID";
	public static final String BEER_TUMB = "thumb";

}