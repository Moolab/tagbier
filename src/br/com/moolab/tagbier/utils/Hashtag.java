package br.com.moolab.tagbier.utils;

public class Hashtag {

	public static String transform(String text) {
		String hash = "#";
		text = text.replaceAll("[^a-z0-9]", " ");
		String[] split = text.split("");
		boolean proximaMaiscula = false;		
		for (String string : split) {
			if (string.equals("#")) {
				continue;
			}
			
			if (proximaMaiscula) {
				
				if (string.equals(" ")) {
					continue;
				}
				
				string = string.toUpperCase();
			}
			
			if (string.equals(" ")) {
				proximaMaiscula = true;
				continue;
			} else {
				proximaMaiscula = false;
			}
			
			hash += string;
		}
		return hash;
	}
}
