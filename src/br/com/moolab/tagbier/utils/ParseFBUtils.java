package br.com.moolab.tagbier.utils;


import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.List;

import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import br.com.moolab.tagbier.R;
import br.com.moolab.tagbier.interfaces.CallbackFBRequest;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.NewPermissionsRequest;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFacebookUtils.Permissions;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;


public class ParseFBUtils {

	private static final List<String> PERMISSIONS_FB_REQUEST = Arrays.asList(		
		Permissions.Extended.ADS_MANAGEMENT,
		Permissions.Extended.PUBLISH_ACTIONS,
		Permissions.Extended.PUBLISH_STREAM,
		Permissions.Extended.PUBLISH_CHECKINS
	);

	private static final List<String> PERMISSIONS_FB_READ = Arrays.asList(
		Permissions.User.PHOTOS
	);

	protected static final String LOGIN_TAG = "LOGIN_UTILS";

	public static final String FACEBOOK_TAG = "FACEBOOK";

	private String userFBID = null;
	
	private static ParseFBUtils instance = new ParseFBUtils();
	
	public interface LoginCallbackUtils {
		public void onLogin(ParseUser user);

		public void onError(ParseException err);
	}
	
	private ParseFBUtils() {
		
	}
	
	public static ParseFBUtils getInstance() {
		return instance;
	}
	
	public void init(Activity activity) {
		
		Parse.initialize(activity.getApplicationContext(), ParseConfig.appID, ParseConfig.clientID);

		/*
		 * Registrando um callback default para push notifications.
		 */
		PushService.setDefaultPushCallback(activity.getApplicationContext(), activity.getClass());

		/*
		 * Solicitando uma sincronização em background de dados pendentes.
		 */
		ParseInstallation.getCurrentInstallation().saveInBackground();

		ParseFacebookUtils.initialize(activity.getString(R.string.app_id));
	}
	
	public boolean isAllowedToPublish() {		
		return ParseFacebookUtils.getSession() != null && ParseFacebookUtils.getSession().isOpened() && Session.isPublishPermission(Permissions.Extended.PUBLISH_STREAM);
	}
	
	/**
	 * Login utilizando a API do Parse
	 * 
	 * @param callback Callback do done
	 * @param activity Quem esta chamando
	 * @param linkAnonymous Usuario anonymous, se tiver, deve ser linkado
	 */
	public void loginFB(final LoginCallbackUtils callback, final Activity activity, final ParseUser linkAnonymous) {		
        
		ParseFacebookUtils.logIn(PERMISSIONS_FB_READ, activity, new LogInCallback() {
			@Override
			public void done(final ParseUser user, ParseException err) {

				if (user == null) {
					Log.d(LOGIN_TAG, "Uh oh. The user cancelled the Facebook login.");
					if (err != null) {
						Log.e(LOGIN_TAG, err.getMessage());
					}
				} else if (user.isNew()) {
					Log.d(LOGIN_TAG, "User signed up and logged in through Facebook!");
				} else {
					Log.d(LOGIN_TAG, "User logged in through Facebook!");
				}				
				
				/**
				 * Link de um usuario anonymous para facebook
				 */
				if (user != null && linkAnonymous != null) {
					if ( ! ParseFacebookUtils.isLinked(linkAnonymous)) {
						ParseFacebookUtils.link(linkAnonymous, activity, new SaveCallback() {
							
							@Override
						    public void done(ParseException ex) {
						      if (ParseFacebookUtils.isLinked(linkAnonymous)) {
						        Log.d(LOGIN_TAG, "Woohoo, user logged in with Facebook!");
						      }
							}
						});
					}
				}
				
				Log.d("access_token", ParseFacebookUtils.getSession().getAccessToken());

				if (user != null && err == null) {
					callback.onLogin(user);
				} else {
					callback.onError(err);
				}
			}
		});
	}
	
	public void requestPermissions(ParseUser user, Activity activity) {
		if (user != null) {			
			if ( ! ParseFacebookUtils.getSession().getPermissions().containsAll(PERMISSIONS_FB_REQUEST)) {
				ParseFacebookUtils.getSession().requestNewPublishPermissions(
					new NewPermissionsRequest(
						activity,
						PERMISSIONS_FB_REQUEST
					)
				);
			}
		}
	}
	
	public void loginAnonymous(final LoginCallbackUtils callback) {
		ParseAnonymousUtils.logIn(new LogInCallback() {
			  @Override
			  public void done(ParseUser user, ParseException err) {
				  if (err != null) {
					  Log.d(LOGIN_TAG, "Anonymous login failed.");
					  callback.onError(err);
				  } else {
					  Log.d(LOGIN_TAG, "Anonymous user logged in.");
					  callback.onLogin(user);
				  }
			  }
		});
	}

	public void logout() {
		if (ParseFacebookUtils.getSession() != null && ParseFacebookUtils.getSession().isOpened()) {
			ParseFacebookUtils.getSession().closeAndClearTokenInformation();
		}
		ParseUser.logOut();		
	}
	
	public void createAvent(final Activity activity,  final Bundle params, final CallbackFBRequest callback) {

		/* make the API call */
		new Request(
		    ParseFacebookUtils.getSession(),
		    "/me/events",
		    params,
		    HttpMethod.POST,
		    new Request.Callback() {
		        public void onCompleted(Response response) {
		        	
		        	if (response.getError() == null) {
		        		Log.d("EVENTO", response.getGraphObject().getInnerJSONObject().toString());
		        		
		        	} else {
		        		Log.e("EVENTO", response.getError().getErrorMessage());
		        	}
		        	
		        	if (checkActivityGood(activity)) {
		        		callback.onCompleteFBRequest(response);
		        	}
		        }
		    }
		).executeAsync();
	}
	
	public void updateEvent(final Activity activity, String eventID, Bundle params, final CallbackFBRequest callback) {		
		/* make the API call */
		new Request(
		    ParseFacebookUtils.getSession(),
		    "/" + eventID,
		    params,
		    HttpMethod.POST,
		    new Request.Callback() {
		        public void onCompleted(Response response) {
		        	
		        	if (response.getError() == null) {
		        		Log.d("EVENTO", response.getGraphObject().getInnerJSONObject().toString());
		        		
		        	} else {
		        		Log.e("EVENTO", response.getError().getErrorMessage());
		        	}
		        	
		        	if (checkActivityGood(activity)) {
		        		callback.onCompleteFBRequest(response);
		        	}
		        }
		    }
		).executeAsync();
	}
	
	public void publishPicture(final Activity activity, final String eventID, final byte[] bitmap, final CallbackFBRequest callback) {
		
		Bundle params = new Bundle();
		params.putByteArray("source", bitmap);
		
		/* make the API call */
		new Request(
		    ParseFacebookUtils.getSession(),
		    "/" + eventID + "/picture",
		    params,
		    HttpMethod.POST,
		    new Request.Callback() {
		        public void onCompleted(Response response) {
		        	
		        	if (response.getError() == null) {
		        		Log.d("EVENTO IMAGEM", response.getGraphObject().getInnerJSONObject().toString());
		        		
		        	} else {
		        		Log.e("EVENTO IMAGEM", response.getError().getErrorMessage());
		        	}
		        	
		        	if (checkActivityGood(activity)) {
		        		callback.onCompleteFBRequest(response);
		        	}
		        }
		    }
		).executeAsync();
	}
	
	public byte[] convertBitmapToByteArray(Bitmap bitmap) {
		 if (bitmap == null) {
			 return null;
		 } else {
			 byte[] b = null;
			 try {
				 ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				 bitmap.compress(CompressFormat.PNG, 0, byteArrayOutputStream);
				 b = byteArrayOutputStream.toByteArray();
			 } catch (Exception e) {
				 e.printStackTrace();
			 }
			 return b;
		 }
	}

	public String getCurrentUserObjectId() {
		return ParseUser.getCurrentUser().getObjectId();
	}
	
	public void getCurrentFBUser(final Activity activity, final CallbackFBRequest callback) {	
		new Request(
			ParseFacebookUtils.getSession(),
		    "/me",
		    null,
		    HttpMethod.GET,
		    new Request.Callback() {
		        public void onCompleted(Response response) {
		        	if (checkActivityGood(activity)) {
		        		callback.onCompleteFBRequest(response);		        		
		        		try {
							userFBID = response.getGraphObject().getInnerJSONObject().getString("id");
						} catch (JSONException e) {
							e.printStackTrace();
						}
		        	}
		        }
		    }
		).executeAsync();
	}
	
	public void getPhoto(final Activity activity, String photoID, final CallbackFBRequest callbackFBRequest) {
		
		new Request(
			ParseFacebookUtils.getSession(),
		    "/" + photoID,
		    null,
		    HttpMethod.GET,
		    new Request.Callback() {
		        public void onCompleted(Response response) {
		        	if (checkActivityGood(activity)) {
		        		callbackFBRequest.onCompleteFBRequest(response);
		        	}
		        }
		    }
		).executeAsync();
	}
	
	public void getPost(final Activity activity, String postID, final CallbackFBRequest callbackFBRequest) {
		
		new Request(
			ParseFacebookUtils.getSession(),
		    "/" + postID,
		    null,
		    HttpMethod.GET,
		    new Request.Callback() {
		        public void onCompleted(Response response) {
		        	if (checkActivityGood(activity)) {
		        		callbackFBRequest.onCompleteFBRequest(response);
		        	}
		        }
		    }
		).executeAsync();
	}
	
	public void getUserPicture(final Activity activity, String userId, final CallbackFBRequest callbackFBRequest) {
		
		Bundle params = new Bundle();
		params.putString("width", "100");
		params.putBoolean("redirect", false);
		params.putString("type", "normal");
		params.putString("height", "100");
		/* make the API call */
		new Request(
			ParseFacebookUtils.getSession(),
		    "/" + userId + "/picture",
		    params,
		    HttpMethod.GET,
		    new Request.Callback() {
		        public void onCompleted(Response response) {
		        	if (checkActivityGood(activity)) {
		        		callbackFBRequest.onCompleteFBRequest(response);
		        	}
		        }
		    }
		).executeAsync();
	}
	
	public boolean checkActivityGood(Activity activity) {
		return activity != null && ! activity.isFinishing();
	}

	public String getCurrentFBUserId() {
		return userFBID;
	}	
	
	public boolean isConnectingToInternet(Context context){
		ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
	    return (networkInfo != null && networkInfo.isConnected());
    }
}