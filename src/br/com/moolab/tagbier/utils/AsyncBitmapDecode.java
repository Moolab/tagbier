package br.com.moolab.tagbier.utils;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;

public class AsyncBitmapDecode extends AsyncTask<String, Void, byte[]> {
    
	public interface CallbackBitmapDecode {
		public void onDecode(byte[] result);
	}
	
	private Context mContext;
	private CallbackBitmapDecode mCallback;

	public AsyncBitmapDecode(final Context context, CallbackBitmapDecode callback) {
		super();
		mContext = context;
		mCallback = callback;
	}
	
	protected byte[] doInBackground(String... urls) {			
		InputStream stream = null;
		Bitmap bitmap = null;
		try {
			if (urls[0] != null) {
				stream = mContext.getContentResolver().openInputStream(Uri.parse(urls[0]));
				bitmap = BitmapFactory.decodeStream(stream);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stream != null) {
					stream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		Bitmap resized = CameraUtil.getResizedBitmap(bitmap, 300);
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		resized.compress(Bitmap.CompressFormat.PNG, 100, outStream);
		byte[] bitMapData = outStream.toByteArray();				
		
		return bitMapData;
	}
	
	public Bitmap getRoundedShape(Bitmap scaleBitmapImage, int size) {
		int targetWidth = size;
		int targetHeight = size;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,Bitmap.Config.ARGB_8888);
		  
		Canvas canvas = new Canvas(targetBitmap);
			Path path = new Path();
			path.addCircle(((float) targetWidth - 1) / 2, ((float) targetHeight - 1) / 2, (Math.min(((float) targetWidth),  ((float) targetHeight)) / 2), Path.Direction.CCW
		);
		  
		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
		sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
		targetHeight), null);
		
		return targetBitmap;
	}

	protected void onPostExecute(byte[] result) {
		mCallback.onDecode(result);
	}
}
