package br.com.moolab.tagbier.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;

@SuppressLint("SimpleDateFormat")
public final class DateGMT {

	public enum EnumDateFormat {
		TO_FB("yyyy-MM-dd'T'HH:mm:ssZ"), TO_FB_DATE_ONLY("yyyy-MM-dd"), TO_USER_TIME("HH:mm:ss"), TO_USER_DATE("dd/MM/yyyy");

		private String format;

		EnumDateFormat(String format) {
			this.format = format;
		}

		public String getFormat() {
			return format;
		}
	}

	public static String format(Date date, EnumDateFormat format) {
		return GetUTCdatetimeAsString(date, format);
	}

	public static String dateToFB(Date date) {
		return GetUTCdatetimeAsString(date, EnumDateFormat.TO_FB);
	}

	private static String GetUTCdatetimeAsString(Date date, EnumDateFormat format) {
		final SimpleDateFormat sdf = new SimpleDateFormat(format.getFormat());
		final String utcTime = sdf.format(date);
		return utcTime;
	}
	
	public static Date convertDateToDate(String StrDate) {
	    Date dateToReturn = null;	    
	    SimpleDateFormat dateFormat = null;
	    if (StrDate.length() > 11) {
	    	dateFormat = new SimpleDateFormat(EnumDateFormat.TO_FB.getFormat());
	    } else {
	    	dateFormat = new SimpleDateFormat(EnumDateFormat.TO_FB_DATE_ONLY.getFormat());
	    }

	    try {
	        dateToReturn = (Date)dateFormat.parse(StrDate);
	    } catch (ParseException e) {
	        e.printStackTrace();
	    }

	    return dateToReturn;
	}
}
