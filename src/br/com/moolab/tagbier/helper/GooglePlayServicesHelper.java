package br.com.moolab.tagbier.helper;

import java.util.ArrayList;
import java.util.List;

public class GooglePlayServicesHelper {
	public final static int STATUS_CHECKING = 0;
	public final static int STATUS_NOT_AVAILABLE = 1;
	public final static int STATUS_AVAILABLE = 2;
	
	private static int servicesStatus = STATUS_CHECKING;
	private static List<GooglePlayServicesListener> listeners = new ArrayList<GooglePlayServicesHelper.GooglePlayServicesListener>();
	
	public static void setServiceStatus(int servicesStatus) {
		GooglePlayServicesHelper.servicesStatus = servicesStatus;
		notifyListeners();
	}
	
	public static int getServiceStatus() {
		return servicesStatus;
	}
	
	public static void addListener(GooglePlayServicesListener listener) {
		listeners.add(listener);
	}
	
	public static void removeListener(GooglePlayServicesListener listener) {
		listeners.remove(listener);
	}
	
	private static void notifyListeners() {
		for (GooglePlayServicesListener listener : listeners) {
			listener.serviceStatusChange(servicesStatus);
		}
	}
	
	public static interface GooglePlayServicesListener {
		public void serviceStatusChange(int servicesStatus);
	}
}
