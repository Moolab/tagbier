/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.moolab.tagbier;

import java.util.List;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import br.com.moolab.tagbier.adapter.BeerSlideAdapter;
import br.com.moolab.tagbier.interfaces.BeerCallback;
import br.com.moolab.tagbier.utils.ParseConfig;
import br.com.moolab.tagbier.utils.Preferences;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQuery.CachePolicy;
import com.parse.ParseUser;

/**
 * Demonstrates a "screen-slide" animation using a {@link ViewPager}. Because {@link ViewPager}
 * automatically plays such an animation when calling {@link ViewPager#setCurrentItem(int)}, there
 * isn't any animation-specific code in this sample.
 *
 * <p>This sample shows a "next" button that advances the user to the next step in a wizard,
 * animating the current screen out (to the left) and the next screen in (from the right). The
 * reverse animation is played when the user presses the "previous" button.</p>
 *
 * @see ScreenSlidePageFragment
 */
public class BeerPageActivity extends ActionBarActivity implements BeerCallback {

    private static final int REQUEST_CODE_NEW_BEER = 0x01;
	private static final int REQUEST_CODE_PLACES = 0x02;
    
    public static String POSITION = "POSITION";

	/**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

	private Bundle mBundle;

	private int mPositionInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#AA000000")));
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#00d0ab")));                
        actionBar.setDisplayShowTitleEnabled(false);
        
        setContentView(R.layout.activity_beer_page);
        
        setSupportProgressBarIndeterminateVisibility(true);
        
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                
        mBundle = (savedInstanceState == null) ? getIntent().getExtras() : savedInstanceState;
		if (mBundle != null) {
			mPositionInt = mBundle.getInt(POSITION);
		}
        
        Parse.initialize(getApplicationContext(),ParseConfig.appID, ParseConfig.clientID);
        
        ParseQuery<ParseUser> queryUser = ParseUser.getQuery();
        if (queryUser != null) {
			queryUser.setCachePolicy(CachePolicy.CACHE_ELSE_NETWORK);
			queryUser.countInBackground(new CountCallback() {
				
				@Override
				public void done(int totalUser, ParseException e) {
					
					if (e != null) {
						Log.e(ParseConfig.TAG_ERROR, e.getMessage());
						return;
					}
					
					Preferences.saveTotalUser(BeerPageActivity.this, totalUser);
					queryBeers();
				}
			});
        } else {
        	queryBeers();
        }
    }    
    
    private void queryBeers() {
		ParseQuery<ParseObject> query = ParseQuery.getQuery(ParseConfig.OBJECT_BEER);
		query.setCachePolicy(CachePolicy.NETWORK_ELSE_CACHE);
		query.whereEqualTo(ParseConfig.POINTER_USER, ParseUser.getCurrentUser());
		query.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> beerList, ParseException e1) {
				
				mPager = (ViewPager) findViewById(R.id.pager);
				mPagerAdapter = new BeerSlideAdapter(getSupportFragmentManager());   
				((BeerSlideAdapter) mPagerAdapter).setData(beerList);
				mPager.setAdapter(mPagerAdapter);
				mPager.setPageTransformer(true, new ZoomOutPageTransformer());
				mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						invalidateOptionsMenu();
					}
				});
				mPager.setCurrentItem(mPositionInt, true);
				
				setSupportProgressBarIndeterminateVisibility(false);
			}
		});
	}
    
    public void onResume() {
    	super.onResume();
    	if (mPagerAdapter != null) {
    		mPagerAdapter.notifyDataSetChanged();
    	}
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.action_add:
			startActivityForResult(new Intent(this, NewBeerActivity.class), REQUEST_CODE_NEW_BEER);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	

	public class ZoomOutPageTransformer implements ViewPager.PageTransformer {
	    private static final float MIN_SCALE = 0.8f;
	    private static final float MIN_ALPHA = 0.8f;

	    public void transformPage(View view, float position) {
	        int pageWidth = view.getWidth();
	        int pageHeight = view.getHeight();

	        if (position < -1) { // [-Infinity,-1)
	            // This page is way off-screen to the left.
	            view.setAlpha(0);

	        } else if (position <= 1) { // [-1,1]
	            // Modify the default slide transition to shrink the page as well
	            float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
	            float vertMargin = pageHeight * (1 - scaleFactor) / 2;
	            float horzMargin = pageWidth * (1 - scaleFactor) / 2;
	            if (position < 0) {
	                view.setTranslationX(horzMargin - vertMargin / 2);
	            } else {
	                view.setTranslationX(-horzMargin + vertMargin / 2);
	            }

	            // Scale the page down (between MIN_SCALE and 1)
	            view.setScaleX(scaleFactor);
	            view.setScaleY(scaleFactor);

	            // Fade the page relative to its size.
	            view.setAlpha(MIN_ALPHA +
	                    (scaleFactor - MIN_SCALE) /
	                    (1 - MIN_SCALE) * (1 - MIN_ALPHA));

	        } else { // (1,+Infinity]
	            // This page is way off-screen to the right.
	            view.setAlpha(0);
	        }
	    }
	}

	@Override
	public void showPlaces(String objectID) {
		Intent intent = new Intent(this, MyPlacesActivity.class);
		intent.putExtra(ParseConfig.OBJCETID, objectID);
		startActivityForResult(intent, REQUEST_CODE_PLACES);
	}
}
