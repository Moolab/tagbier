package br.com.moolab.tagbier;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.moolab.tagbier.interfaces.CallbackFBRequest;
import br.com.moolab.tagbier.utils.AsyncBitmapDecode;
import br.com.moolab.tagbier.utils.AsyncBitmapDecode.CallbackBitmapDecode;
import br.com.moolab.tagbier.utils.CameraUtil;
import br.com.moolab.tagbier.utils.Fonts;
import br.com.moolab.tagbier.utils.Hashtag;
import br.com.moolab.tagbier.utils.ParseConfig;
import br.com.moolab.tagbier.utils.ParseFBUtils;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.OpenGraphAction;
import com.facebook.model.OpenGraphObject;
import com.facebook.widget.FacebookDialog;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQuery.CachePolicy;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

public class NewBeerActivity extends ActionBarActivity implements StatusCallback {

	
	private Bundle mBundle;
	
	private EditText mTipo;
	private EditText mTitulo;
	
	private Button mActionSave;
	
	private ImageView mFoto;
	
	private Typeface robotoCondensed;
	private Typeface robotoNormal;
	private EditText mNewTag;

	private static final int ACTION_CODE_CAM = 0x01;
	private static final int REQUEST_PUBLISH = 0x02;
	private static final int REQUEST_GALLERY_PICTURE = 0x03;

	protected static final int REQUEST_WIFI = 0x04;

	private String momentPhoto;
	private ImageButton mAddTag;
	private LinearLayout mTags;
	private ProgressBar mLoadingFoto;
	private CheckBox mPostFB;

	protected byte[] mBitmapData;

	private UiLifecycleHelper uiHelper;

	private ImageView mActionFoto;

	private ImageView mActionChoose;

	private View mNoConnection;

	private ScrollView mScroll;

	private Typeface robotoThin;

	private int tagAlpha = 255;

	private ParseObject beer;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#0987b6")));
		actionBar.setDisplayShowTitleEnabled(false);
		
		setContentView(R.layout.activity_new_beer);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);		
		setSupportProgressBarIndeterminateVisibility(false);
		
		mBundle = (savedInstanceState == null) ? getIntent().getExtras() : savedInstanceState;
		
		if (mBundle != null) {
			String foto = mBundle.getString(ParseConfig.BEER_FOTO);
			momentPhoto = foto;
		}
		
		Parse.initialize(getApplicationContext(), ParseConfig.appID, ParseConfig.clientID);
		if (mBundle != null && mBundle.getString(ParseConfig.OBJCETID) != null) {
			ParseQuery<ParseObject> query = ParseQuery.getQuery(ParseConfig.OBJECT_BEER);
			query.setCachePolicy(CachePolicy.CACHE_THEN_NETWORK);		
			query.getInBackground(mBundle.getString(ParseConfig.OBJCETID), new GetCallback<ParseObject>() {
				
				@Override
				public void done(ParseObject tipo, ParseException e) {				
					
					if (e != null) {
						Log.e(ParseConfig.TAG_ERROR, e.getMessage());
					} else if (tipo != null) {
					  	mTipo.setText(tipo.getString(ParseConfig.BEER_TIPO));
					  	setSupportProgressBarIndeterminateVisibility(false);
					}
				}
			});
		}
		
		uiHelper = new UiLifecycleHelper(this, this);
	    uiHelper.onCreate(savedInstanceState);
		
		onCreateView();
	}

	private void loadPicture(String foto) {	
		
		if (foto == null) {
			return;
		}
		
		mActionChoose.setVisibility(View.INVISIBLE);
		mActionFoto.setVisibility(View.INVISIBLE);
				
		mLoadingFoto.setVisibility(View.VISIBLE);
		mFoto.setVisibility(View.INVISIBLE);
		
		CallbackBitmapDecode callbackBitmap = new CallbackBitmapDecode() {					

			@Override
			public void onDecode(final byte[] bitMapData) {
				if (bitMapData != null) {
					
					mBitmapData = bitMapData;
					
					Picasso.with(NewBeerActivity.this).load(momentPhoto).resizeDimen(R.dimen.grid_beer_item_width, R.dimen.grid_beer_new_item).centerInside().into(mFoto);
					mFoto.setVisibility(View.VISIBLE);
					mLoadingFoto.setVisibility(View.INVISIBLE);
					
				} else {
					setSupportProgressBarIndeterminateVisibility(false);
				}
			}
		};

		AsyncBitmapDecode asyncDecode = new AsyncBitmapDecode(this, callbackBitmap);
		asyncDecode.execute(foto);
	
	}
	
	private void onCreateView() {
		robotoCondensed = Fonts.getInstance().getRobotoCondensed(getAssets());
		robotoThin = Fonts.getInstance().getRobotoThin(getAssets());
		robotoNormal = Fonts.getInstance().getRobotoRegular(getAssets());	
		
		mLoadingFoto = (ProgressBar) findViewById(R.id.loading_foto);
		
		mNoConnection = (View) findViewById(R.id.no_connection);
		mScroll = (ScrollView) findViewById(R.id.scroll);
		
		mFoto = (ImageView) findViewById(R.id.foto);
		mActionFoto = (ImageView) findViewById(R.id.action_cam);
		mActionFoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				recordMomentPhoto();	
			}
		});
		mActionChoose = (ImageView) findViewById(R.id.action_choose);
		mActionChoose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				choosePicture();
			}
		});
		
		TextView dica = (TextView) findViewById(R.id.dica_fb);		
		dica.setTypeface(robotoCondensed);
		
		mTitulo = (EditText) findViewById(R.id.titulo);		
		mTitulo.setTypeface(robotoCondensed);
		
		mTipo = (EditText) findViewById(R.id.tipo);		
		mTipo.setTypeface(robotoNormal);
		
		mPostFB = (CheckBox) findViewById(R.id.action_fb);		
		if (FacebookDialog.canPresentOpenGraphActionDialog(getApplicationContext(), FacebookDialog.OpenGraphActionDialogFeature.OG_ACTION_DIALOG)) {
			mPostFB.setTypeface(robotoCondensed);
		} else {
			mPostFB.setVisibility(View.GONE);
		}
		
		mNewTag = (EditText) findViewById(R.id.new_tag);		
		mNewTag.setTypeface(robotoCondensed);
		
		mTags = (LinearLayout) findViewById(R.id.tags);
		
		mAddTag = (ImageButton) findViewById(R.id.action_add_tag);
		mAddTag.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				addNewTag();
			}
		});
		
		mActionSave = (Button) findViewById(R.id.action_save);					
		mActionSave.setTypeface(robotoNormal);		
		mActionSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				mActionSave.setEnabled(false);
				
				if (momentPhoto == null) {
					Toast.makeText(NewBeerActivity.this, getString(R.string.no_foto), Toast.LENGTH_SHORT).show();
					return;
				}
				
				save();					
			}
		});
		
		mNewTag.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction()==0) {					
					mScroll.fullScroll(ScrollView.FOCUS_DOWN);
					addNewTag();
					mNewTag.requestFocus();
				}
				return false;
			}
		});
		
		/**
		 * No Connection
		 */
		
		Button mConnect = (Button) findViewById(R.id.action_connect);					
		mConnect.setTypeface(robotoNormal);
		
		Button mCancel = (Button) findViewById(R.id.action_cancel);					
		mCancel.setTypeface(robotoNormal);		
		
		TextView mConnectMsg = (TextView) findViewById(R.id.msg_connect);					
		mConnectMsg.setTypeface(robotoCondensed);
		
		TextView mNoConnectMsg = (TextView) findViewById(R.id.msg_no_connection);					
		mNoConnectMsg.setTypeface(robotoThin);
		
		mConnect.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), REQUEST_WIFI);
			}
		});
		mCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		checkConnection();
	}
	
	private View getHashTagView(final LinearLayout tags, String tag) {
		TextView child = (TextView) getLayoutInflater().inflate(R.layout.hashtag, tags, false);
		child.setTypeface(robotoCondensed);
		child.setText(tag);
		child.setTag(tags.getChildCount() - 2);
		child.setBackgroundColor(Color.argb(tagAlpha, 0, 154, 140));
		tagAlpha -= 60;
		child.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				tags.removeViewAt((Integer) v.getTag());
				tagAlpha += 60;
			}
		});
		return child;
	}

	@Override
	public void onResume() {
		super.onResume();
		
		if (mBundle == null) {
			mBundle = new Bundle();
		}
		
		uiHelper.onResume();
		
		loadPicture(momentPhoto);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		if (mBundle != null && outState != null) {
			outState.putAll(mBundle);
		}
		
		uiHelper.onSaveInstanceState(outState);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.new_beer, menu);
		return true;		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.action_cam:
			recordMomentPhoto();
			return true;
		case R.id.action_choose:
			choosePicture();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void choosePicture() {
		Intent pictureActionIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
        pictureActionIntent.setType("image/*");
        pictureActionIntent.putExtra("return-data", true);
        startActivityForResult(pictureActionIntent, REQUEST_GALLERY_PICTURE);
	}

	private void recordMomentPhoto() {
		PackageManager pm = NewBeerActivity.this.getPackageManager();
		if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA) && CameraUtil.isIntentAvailable(NewBeerActivity.this, MediaStore.ACTION_IMAGE_CAPTURE)) {
			Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			takePictureIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
			startActivityForResult(takePictureIntent, ACTION_CODE_CAM);
		} else {
			Toast.makeText(NewBeerActivity.this, getString(R.string.msg_sorry_cam), Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);

		setSupportProgressBarIndeterminateVisibility(false);
		
		if (requestCode == ACTION_CODE_CAM || requestCode == REQUEST_GALLERY_PICTURE) {
			momentPhoto = null;
			if (resultCode == Activity.RESULT_OK) {
				mActionFoto.setVisibility(View.GONE);
				momentPhoto = intent.getData().toString();
				mBundle.putString(ParseConfig.BEER_FOTO, momentPhoto);
			}
		} else if (requestCode == REQUEST_PUBLISH) {
			uiHelper.onActivityResult(requestCode, resultCode, intent, new FacebookDialog.Callback() {
				@Override
				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
					finish();
				}
				
				@Override
				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
					String postId = FacebookDialog.getNativeDialogPostId(data);
					beer.add(ParseConfig.BEER_POST_ID, postId);
					beer.saveInBackground(new SaveCallback() {
						
						@Override
						public void done(ParseException arg0) {
							finish();	
						}
					});					
				}
			});
		} else if (requestCode == REQUEST_WIFI) {
			checkConnection();
		} else {
			ParseFacebookUtils.finishAuthentication(requestCode, resultCode, intent);
		}
	}
	
	private void save() {
		
		if (mTitulo.getText().toString() == null || mTitulo.getText().toString().length() == 0) {
			Toast.makeText(this, getString(R.string.msg_no_titulo), Toast.LENGTH_SHORT).show();
			return;
		}
		
		checkConnection();
		
		final ArrayList<String> tags = new ArrayList<String>();
		for (int x = 0 ; x < mTags.getChildCount() ; x++) {
			TextView tag = (TextView) mTags.getChildAt(x);
			tags.add(tag.getText().toString());
		}
		
		setSupportProgressBarIndeterminateVisibility(true);
		
		ParseUser currentUser = ParseUser.getCurrentUser();		
		beer = new ParseObject(ParseConfig.OBJECT_BEER);
		beer.put(ParseConfig.BEER_TIPO, mTipo.getText().toString());
		beer.put(ParseConfig.BEER_TITULO, mTitulo.getText().toString());
		beer.put(ParseConfig.POINTER_USER, currentUser);
		beer.put(ParseConfig.BEER_QUANDO, Calendar.getInstance(Locale.getDefault()).getTime());
		beer.addAll(ParseConfig.BEER_HASHTAGS, tags);
				
		saveFacebook(momentPhoto, beer.getString(ParseConfig.BEER_TITULO), beer.getString(ParseConfig.BEER_TIPO), beer.getJSONArray(ParseConfig.BEER_HASHTAGS), tags, beer);
	}

	private void checkConnection() {
		if ( ! ParseFBUtils.getInstance().isConnectingToInternet(this)) {
			mNoConnection.setVisibility(View.VISIBLE);
			mScroll.setVisibility(View.GONE);
			return;
		} else {
			mNoConnection.setVisibility(View.GONE);
			mScroll.setVisibility(View.VISIBLE);
		}
	}
	
	public void saveFacebook(final String picture, final String titulo, final String tipo, final JSONArray tags, final ArrayList<String> tagsList, final ParseObject beer) {
		
		ParseFBUtils.getInstance().requestPermissions(ParseUser.getCurrentUser(), this);
		
		Bundle param = new Bundle();
		param.putString("caption", titulo + " - " + tipo);
		param.putByteArray("picture", mBitmapData);
		param.putBoolean("no_story", true);
		param.putStringArrayList("caption_tags", tagsList);
		
		new Request(
		    ParseFacebookUtils.getSession(),
		    "me/photos",
		    param,
		    HttpMethod.POST,
		    new Request.Callback() {
		        public void onCompleted(Response response) {
		        	
		        	if (response.getError() != null) {
		        		Log.e("PHOTO", response.getError().getErrorMessage());
		        	}
		        	
		        	if (response.getGraphObject() == null) {
		        		Toast.makeText(NewBeerActivity.this, getString(R.string.msg_sorry_cam), Toast.LENGTH_SHORT).show();
		        		return;
		        	}
		        	
		        	JSONObject innerJSONObject = response.getGraphObject().getInnerJSONObject();
		        			        	
		        	try {
		        		String photoId = innerJSONObject.getString("id");
		        		beer.put(ParseConfig.BEER_PHOTO_ID, photoId);
		        		
		        		/*
		        		 * Request photo
		        		 */
		        		ParseFBUtils.getInstance().getPhoto(NewBeerActivity.this, photoId, new CallbackFBRequest() {
							
							@Override
							public void onCompleteFBRequest(Response response) {
								
								JSONObject photo = null;
								if (response.getError() == null) {
					        		photo = response.getGraphObject().getInnerJSONObject();					        		
					        	} else {
					        		Log.e("PHOTO", response.getError().getErrorMessage());
					        		return;
					        	}
								
			           			try {
									beer.put(ParseConfig.BEER_FOTO, photo.getString("source"));
					           		beer.put(ParseConfig.BEER_TUMB, photo.getString("picture"));
					           		
					           		beer.saveInBackground();
					        		
					           		if (mPostFB.isChecked()) {
					           			OpenGraphObject bier = OpenGraphObject.Factory.createForPost("tagbier:bier");
					           			bier.setTitle(titulo);
					           			bier.setProperty("image", picture);
					           			bier.setUrl(photo.getString("link"));
					           			bier.getData().setProperty("tipo", tipo);
					           			bier.getData().setProperty("hashtags", tags);
					           			
					           			OpenGraphAction action = GraphObject.Factory.create(OpenGraphAction.class);
					           			action.setProperty("bier", bier);
					           			
						           		FacebookDialog shareDialog = new FacebookDialog.OpenGraphActionDialogBuilder(NewBeerActivity.this, action, "tagbier:drink", "bier")
						           		.setRequestCode(REQUEST_PUBLISH)						           		
						           		.build();
						           		uiHelper.trackPendingDialogCall(shareDialog.present());
						           		
					           		} else {
					           			finish();
					           		}
				           		} catch (JSONException e) {
				           			Log.e("PHOTO", e.getMessage());
				           		}
							}
						});
		        		
	               } catch (JSONException e) {
	                   Log.e("PHOTO", "JSON error "+ e.getMessage());
	                   finish();
	               }
		        }
		    }
		).executeAsync();
	}	

	@Override
	public void call(Session session, SessionState state, Exception exception) {
		
	}	

	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}

	private void addNewTag() {
		
		if (mTags.getChildCount() == 3) {
			Toast.makeText(this, getString(R.string.msg_max_tag), Toast.LENGTH_SHORT).show();
			return;
		}
		
		String string = mNewTag.getText().toString();
		if (string != null && string.length() > 0) {
			View hashTagView = getHashTagView(mTags, Hashtag.transform(string));
			mTags.addView(hashTagView);
			mNewTag.setText("");
		}
	}
}