package br.com.moolab.tagbier;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import br.com.moolab.tagbier.fragment.ErrorDialogFragment;
import br.com.moolab.tagbier.helper.GooglePlayServicesHelper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;

public abstract class GooglePlayFragmentActivity extends ActionBarActivity implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {
	
	private static final String DIALOG_GOOGLE_SERVICES = "DIALOG_GS";
	protected final static int GOOGLE_PS_CONNECTION_ERROR = 1000;
	
	protected void googlePlayServicesConnected(boolean raiseDialog) {
		GooglePlayServicesHelper.setServiceStatus(GooglePlayServicesHelper.STATUS_CHECKING);
		
		int result = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		
		if (result == ConnectionResult.SUCCESS) {
			GooglePlayServicesHelper.setServiceStatus(GooglePlayServicesHelper.STATUS_AVAILABLE);
			return;
		}
		
		if (!raiseDialog) {
			GooglePlayServicesHelper.setServiceStatus(GooglePlayServicesHelper.STATUS_NOT_AVAILABLE);
			return;
		}
		
		Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
				result,
				this,
				GOOGLE_PS_CONNECTION_ERROR
			);
		
		if (errorDialog != null) {
			ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();
			errorDialogFragment.setDialog(errorDialog);
			errorDialogFragment.show(
					getSupportFragmentManager(),
					DIALOG_GOOGLE_SERVICES
				);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
			case GOOGLE_PS_CONNECTION_ERROR:
				// Google Play Services returned something
				if (resultCode == Activity.RESULT_OK) {
					GooglePlayServicesHelper.setServiceStatus(GooglePlayServicesHelper.STATUS_AVAILABLE);
				} else {
					GooglePlayServicesHelper.setServiceStatus(GooglePlayServicesHelper.STATUS_NOT_AVAILABLE);
				}
				break;
	
			default:
				// Who knows?
				break;
		}
	}

}
