/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http:www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.moolab.tagbier;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import br.com.moolab.tagbier.fragment.DialogNoGooglePlayServicesFragment;
import br.com.moolab.tagbier.helper.GooglePlayServicesHelper;
import br.com.moolab.tagbier.helper.GooglePlayServicesHelper.GooglePlayServicesListener;
import br.com.moolab.tagbier.interfaces.CallbackFBRequest;
import br.com.moolab.tagbier.utils.Fonts;
import br.com.moolab.tagbier.utils.ParseConfig;
import br.com.moolab.tagbier.utils.ParseFBUtils;

import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

public class MyPlacesActivity extends GooglePlayFragmentActivity implements GooglePlayServicesListener, StatusCallback {  

    private static final int REQUEST_PIN = 0x01;
	protected static final int REQUEST_WIFI = 0x02;
	private Bundle mBundle;
	
	private Typeface robotoCondensed;
	private Typeface robotoNormal;
	private Typeface robotoThin;
	
	private MapView mMapView;
	private GoogleMap mMap;
	private LocationClient locationClient;
	private Location location;
	private View mNoConnection;
	private UiLifecycleHelper uiHelper;
	protected ParseObject mBeer;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#AA000000")));
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#00d0ab")));
        actionBar.setDisplayShowTitleEnabled(false);
        
        setContentView(R.layout.activity_places);
        
        Parse.initialize(getApplicationContext(),ParseConfig.appID, ParseConfig.clientID);
        
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
				
		mBundle = (savedInstanceState == null) ? getIntent().getExtras() : savedInstanceState;
		
		robotoCondensed = Fonts.getInstance().getRobotoCondensed(getAssets());
		robotoNormal = Fonts.getInstance().getRobotoRegular(getAssets());
		robotoThin = Fonts.getInstance().getRobotoThin(getAssets());
		
		// Gets the MapView from the XML layout and creates it
		mMapView = (MapView) findViewById(R.id.map);
		mMapView.onCreate(savedInstanceState);
		
		mMap = mMapView.getMap();
		
		try {
			// so we can use the CameraUpdateFactory
		    MapsInitializer.initialize(this);
		} catch (GooglePlayServicesNotAvailableException e) {
		    e.printStackTrace();
		}
		
		// first we check for Google Play services status
		if (GooglePlayServicesHelper.getServiceStatus() == GooglePlayServicesHelper.STATUS_AVAILABLE) {
			
		} else {
			// let's listen to the status change
			GooglePlayServicesHelper.addListener(this);
			// and try to connect
			googlePlayServicesConnected(true);
		}
		
		uiHelper = new UiLifecycleHelper(this, this);
	    uiHelper.onCreate(savedInstanceState);
	    
	    /**
		 * No Connection
		 */
		
		Button mConnect = (Button) findViewById(R.id.action_connect);					
		mConnect.setTypeface(robotoNormal);
		
		Button mCancel = (Button) findViewById(R.id.action_cancel);					
		mCancel.setTypeface(robotoNormal);		
		
		TextView mConnectMsg = (TextView) findViewById(R.id.msg_connect);					
		mConnectMsg.setTypeface(robotoCondensed);
		
		TextView mNoConnectMsg = (TextView) findViewById(R.id.msg_no_connection);					
		mNoConnectMsg.setTypeface(robotoThin);
		
		mConnect.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), REQUEST_WIFI);
			}
		});
		mCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
//		setUpMeOnMap();
    }
	
	protected void setUpMeOnMap() {
		locationClient = new LocationClient(this, this, this);
		locationClient.connect();
	}
	
    public void onResume() {
    	mMapView.onResume();
		
    	super.onResume();
		
    	uiHelper.onResume();
    	
    	setSupportProgressBarIndeterminateVisibility(true);
    	
    	if (GooglePlayServicesHelper.getServiceStatus() == GooglePlayServicesHelper.STATUS_AVAILABLE) {
			
		}
    	
    	ParseQuery<ParseObject> query = ParseQuery.getQuery(ParseConfig.OBJECT_BEER);
    	query.getInBackground(mBundle.getString(ParseConfig.OBJCETID), new GetCallback<ParseObject>() {						

			@Override
			public void done(ParseObject beer, ParseException e) {
				
				mBeer = beer;
				
				if (e != null) {
					Log.e(ParseConfig.TAG_ERROR, e.getMessage());
					return;
				}
				
				String postID = beer.getString(ParseConfig.BEER_POST_ID);
				if (postID != null) {
					pointBeer(postID, true);
				} 
			}
		});    	    	
    }

	private void pointBeer(String postID, final boolean last) {
		
		ParseFBUtils.getInstance().getPost(this, postID, new CallbackFBRequest() {
 			
 			@Override
 			public void onCompleteFBRequest(Response response) { 			  	
 			  	
 				if (last) {
 					setSupportProgressBarIndeterminateVisibility(false);
 				}
 				
 			  	if (response == null) {
 			  		return;
 			  	}
 		  		
 			  	if (response.getGraphObject() == null) {
 			  		return;
 			  	}
 			  	
 			  	if (response.getGraphObject().getInnerJSONObject() == null) {
 		  			return;
 		  		}
 		  		
 		  		try {
 					JSONObject place = response.getGraphObject().getInnerJSONObject().getJSONObject("place");
					JSONObject location = place.getJSONObject("location");
 					
 					LatLng startLatLng = new LatLng(location.getLong("latitude"), location.getLong("longitude"));
 					
 					mMap.addMarker(
 						new MarkerOptions()
 							.icon(BitmapDescriptorFactory.fromResource(R.drawable.point_beer))
 							.position(startLatLng)
 							.title(place.getString("name"))
 					);		
 					
 					LatLngBounds latLngBounds = new LatLngBounds(
						new LatLng(startLatLng.latitude, startLatLng.longitude),
						new LatLng(startLatLng.latitude, startLatLng.longitude)
					);
					mMap.moveCamera(
						CameraUpdateFactory.newLatLngBounds(
							latLngBounds,
							getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin)
						)
					);
 					
 				} catch (JSONException e) {
 					Log.e("JSON Location", e.getMessage());
 				}			  	
 			}
 		});
	}
    
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (outState != null && mBundle != null) {
			outState.putAll(mBundle);
		}
		
		uiHelper.onSaveInstanceState(outState);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);

		setSupportProgressBarIndeterminateVisibility(false);
		
		if (requestCode == REQUEST_PIN) {
			uiHelper.onActivityResult(requestCode, resultCode, intent, new FacebookDialog.Callback() {
				@Override
				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
					finish();
				}
				
				@Override
				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
					String postId = FacebookDialog.getNativeDialogPostId(data);
					mBeer.add(ParseConfig.BEER_POST_ID, postId);
					mBeer.saveInBackground(new SaveCallback() {
						
						@Override
						public void done(ParseException arg0) {
							
						}
					});					
				}
			});
		} else if (requestCode == REQUEST_WIFI) {
			checkConnection();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.places, menu);
		return true;		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {		
		
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
//		case R.id.action_pin:
//			if (checkConnection()) {
//       			OpenGraphAction action = GraphObject.Factory.create(OpenGraphAction.class);
//           		FacebookDialog shareDialog = new FacebookDialog.OpenGraphActionDialogBuilder(MyPlacesActivity.this, action, "tagbier:drink")
//           		.setRequestCode(REQUEST_PIN)	           		
//           		.build();
//           		uiHelper.trackPendingDialogCall(shareDialog.present());
//			}
//			return true;		
		}
		return super.onOptionsItemSelected(item);
	}
	
	private boolean checkConnection() {
		
		if (mNoConnection == null) {
			mNoConnection = (View) findViewById(R.id.no_connection);
		}
		
		if ( ! ParseFBUtils.getInstance().isConnectingToInternet(this)) {
			mNoConnection.setVisibility(View.VISIBLE);
			return true;
		} else {
			mNoConnection.setVisibility(View.GONE);
		}
		return false;
	}	

	@Override
	public void onConnected(Bundle bundle) {
		location = locationClient.getLastLocation();
		myLocation();
	}

	private void myLocation() {
		LatLng startLatLng = new LatLng(location.getLatitude(), location.getLongitude());
		
		mMap.addMarker(
			new MarkerOptions()
				.position(startLatLng)
				.title(getString(R.string.app_name))
		);		
	}	

	@Override
	public void onDisconnected() {}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {}

	@Override
	public void serviceStatusChange(int servicesStatus) {
		if (servicesStatus != GooglePlayServicesHelper.STATUS_CHECKING) {
			GooglePlayServicesHelper.removeListener(this);
		}
		
		if (servicesStatus == GooglePlayServicesHelper.STATUS_AVAILABLE) {
			
		}
		
		if (servicesStatus == GooglePlayServicesHelper.STATUS_NOT_AVAILABLE) {
			DialogNoGooglePlayServicesFragment dialog = new DialogNoGooglePlayServicesFragment();
			dialog.show(getSupportFragmentManager(), DialogNoGooglePlayServicesFragment.DIALOG_NO_PLAY_SERVICES);
		}
	}
	
	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}

	@Override
	public void call(Session session, SessionState state, Exception exception) {
		
	}
}
