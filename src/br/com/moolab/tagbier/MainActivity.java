/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http:www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.moolab.tagbier;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import br.com.moolab.tagbier.adapter.NavigationDrawerAdapter;
import br.com.moolab.tagbier.fragment.BeerGridFragment;
import br.com.moolab.tagbier.fragment.CallbackGrid;
import br.com.moolab.tagbier.utils.ParseConfig;

import com.parse.Parse;
import com.parse.ParseUser;

public class MainActivity extends ActionBarActivity implements CallbackGrid {

    public static final int REQUEST_CODE_NEW_BEER = 0x01;
	private static final int REQUEST_CODE_PAGER = 0x02;
	protected CharSequence mTitle;
	private DrawerLayout mDrawerLayout;
	private NavigationDrawerAdapter navigationAdapter;
	private ActionBarDrawerToggle mDrawerToggle;
	protected int mNavSelected;
	private AbsListView mDrawerList;	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#AA000000")));
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#00d0ab")));
        
        actionBar.setDisplayShowTitleEnabled(false);
        setContentView(R.layout.activity_main);
        
        Parse.initialize(getApplicationContext(),ParseConfig.appID, ParseConfig.clientID);
		
		mTitle = getString(R.string.app_name);
	
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		navigationAdapter = new NavigationDrawerAdapter(this);
		mDrawerList.setAdapter(navigationAdapter);
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_navigation_drawer, /* nav drawer icon to replace 'Up' caret */
		R.string.app_name, /* "open drawer" description */
		R.string.app_name /* "close drawer" description */
		) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				getSupportActionBar().setTitle(mTitle);
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				getSupportActionBar().setTitle(getString(R.string.app_name));
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		
    	JSONArray navList = new JSONArray();    	
    	
    	JSONObject navLogout = new JSONObject();
    	try {
    		navLogout.put(NavigationDrawerAdapter.THUMB, R.drawable.ic_facebook);
    		navLogout.put(NavigationDrawerAdapter.TITLE, getString(R.string.action_logout));
		} catch (JSONException e) {
			Log.e("Erro JSON", e.getMessage());
		}
    	navList.put(navLogout);
    	
    	JSONObject navFeedback = new JSONObject();
    	try {
    		navFeedback.put(NavigationDrawerAdapter.THUMB, R.drawable.ic_action_mail);
    		navFeedback.put(NavigationDrawerAdapter.TITLE, getString(R.string.action_mail));
		} catch (JSONException e) {
			Log.e("Erro JSON", e.getMessage());
		}
    	navList.put(navFeedback);
    	
    	
		navigationAdapter.setData(navList);
        navigationAdapter.notifyDataSetChanged();		     

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
    }
    
    public void onResume() {    	    	
    	super.onResume();
    	
    	BeerGridFragment beerListFragment = new BeerGridFragment();
		getSupportFragmentManager().beginTransaction().replace(R.id.beers, beerListFragment).commit();
    }   	
    
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
		
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);
		}
	}
	
	/** Swaps fragments in the main content view */
	private void selectItem(int position) {
		
		if (position == 0) {
			ParseUser.logOut();
			finish();
			return;
		} else if (position == 1) {
			Intent i = new Intent(Intent.ACTION_SEND);
			i.setType("message/rfc822");
			i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"tagbier@moolab.com.br"});
			try {
			    startActivity(Intent.createChooser(i, "Enviar Feedback"));
			} catch (android.content.ActivityNotFoundException ex) {
			    Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
			}
			return;
		}
		
		mNavSelected = position;
		
		mDrawerList.setItemChecked(position, true);
		
		if (navigationAdapter != null) {
			JSONObject item = navigationAdapter.getItem(position);
			if (item != null) {
				try {
					setTitle(item.getString(NavigationDrawerAdapter.TITLE));
				} catch (JSONException e) {
					Log.e("Erro JSON", e.getMessage());
				}
			}
		}
		
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;		
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		
		switch (item.getItemId()) {
		case R.id.action_add:
			startActivityForResult(new Intent(this, NewBeerActivity.class), REQUEST_CODE_NEW_BEER);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void choosed(String rowID, int position) {
		Intent intent = new Intent(this, BeerPageActivity.class);
		intent.putExtra(BeerPageActivity.POSITION, position);
		startActivityForResult(intent, REQUEST_CODE_PAGER);
	}
}
