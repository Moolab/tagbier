package br.com.moolab.tagbier.interfaces;

import com.facebook.Response;


public interface CallbackFBRequest {

	void onCompleteFBRequest(Response response);
}
