package br.com.moolab.tagbier.interfaces;

import android.support.v4.app.DialogFragment;

public interface ListenerDialogs {

	public void onDialogPositiveClick(DialogFragment dialog);

	public void onDialogNegativeClick(DialogFragment dialog);
}
