package br.com.moolab.tagbier;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import br.com.moolab.tagbier.utils.Fonts;
import br.com.moolab.tagbier.utils.ParseFBUtils;
import br.com.moolab.tagbier.utils.ParseFBUtils.LoginCallbackUtils;

import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

public class LoginActivity extends ActionBarActivity {

	private Button mBtnFB;
	private ParseFBUtils fbUtil;
//	private Button mBtnAnonymous;
	private TextView mError;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Needs to be called before setting the content view
	    supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
	    
	    getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
	    ActionBar actionBar = getSupportActionBar();
	    actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
	    actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
		actionBar.setDisplayShowTitleEnabled(false);
		
		setContentView(R.layout.activity_login);
		
		setSupportProgressBarIndeterminateVisibility(false);
		
		fbUtil = ParseFBUtils.getInstance();
		fbUtil.init(this);
		
		final ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser != null && currentUser.isAuthenticated()) {
			entryApp();
		}
		
		mError = (TextView) findViewById(R.id.error);
		mError.setTypeface(Fonts.getInstance().getRobotoMedium(getAssets()));
		
		Typeface robotoCondensed = Fonts.getInstance().getRobotoCondensed(getAssets());
		mBtnFB = (Button) findViewById(R.id.btn_facebook);
		mBtnFB.setTypeface(robotoCondensed);
		mBtnFB.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				setProgressBarIndeterminateVisibility(Boolean.TRUE); 
				
				fbUtil.loginFB(new LoginCallbackUtils() {
					
					@Override
					public void onLogin(ParseUser user) {
						setProgressBarIndeterminateVisibility(Boolean.FALSE);
						entryApp();
					}
					
					@Override
					public void onError(ParseException err) {
						setProgressBarIndeterminateVisibility(Boolean.FALSE);
						mError.setText(err.getMessage());
						mError.setVisibility(View.VISIBLE);
					}
				}, LoginActivity.this, currentUser);
			}
		});
	}

	private void entryApp() {
		startActivity(new Intent(this, MainActivity.class));
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
	  ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}
}