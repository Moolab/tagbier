package br.com.moolab.tagbier.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import br.com.moolab.tagbier.R;
import br.com.moolab.tagbier.interfaces.ListenerDialogs;
import br.com.moolab.tagbier.utils.Fonts;

public abstract class DialogJoyFragment extends DialogFragment {
	
	ListenerDialogs mListener;
	
	private Builder mBuilder;
	
	protected int icon = -1;
	protected int title = -1;
	protected int message = -1;
	protected int positiveLabel = -1;
	protected int negativeLabel = -1;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (ListenerDialogs) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement Dialogs");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View viewInflated = (View) inflater.inflate(R.layout.dialog_no_connection, null);
		
		mBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Dialogs));
		mBuilder.setView(viewInflated);
		mBuilder.setIcon(icon);
		mBuilder.setTitle(getString(title));
		
		if (positiveLabel > -1) {
			mBuilder.setPositiveButton(getString(positiveLabel), new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mListener.onDialogPositiveClick(DialogJoyFragment.this);
				}
			});
		}
		
		if (negativeLabel > -1) {
			mBuilder.setNegativeButton(getString(negativeLabel), new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mListener.onDialogNegativeClick(DialogJoyFragment.this);
				}
			});
		}
		
		final AlertDialog dialog = mBuilder.create();		
		
		Typeface robotoCondensed = Fonts.getInstance().getRobotoLight(getActivity().getAssets());

		TextView msg = (TextView) viewInflated.findViewById(R.id.msg);
		msg.setText(message);
		msg.setTypeface(robotoCondensed);		
		
		return dialog;
	}
}
