/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.moolab.tagbier.fragment;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.moolab.tagbier.MainActivity;
import br.com.moolab.tagbier.NewBeerActivity;
import br.com.moolab.tagbier.R;
import br.com.moolab.tagbier.interfaces.CallbackFBRequest;
import br.com.moolab.tagbier.utils.Fonts;
import br.com.moolab.tagbier.utils.ParseConfig;
import br.com.moolab.tagbier.utils.ParseFBUtils;

import com.facebook.Response;
import com.google.android.gms.maps.model.LatLng;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQuery.CachePolicy;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

public class BeerGridFragment extends Fragment implements AdapterView.OnItemClickListener {	
	
	private int mImageThumbSize;
	private int mImageThumbSpacing;
	private ImageAdapter mAdapter;
	private CallbackGrid mCallbacks = sDummyCallbacks;

	private Bundle mBundle;

	private GridView mGridView;
	private View mEmpty;
	
//	private MapView mMapView;
//	private GoogleMap mMap;

	private static CallbackGrid sDummyCallbacks = new CallbackGrid() {

		@Override
		public void choosed(String rowID, int position) {}
	};

	/**
	 * Empty constructor as per the Fragment documentation
	 */
	public BeerGridFragment() {}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (!(activity instanceof CallbackGrid)) {
			throw new IllegalStateException("Activity must implement fragment's callbacks.");
		}

		mCallbacks = (CallbackGrid) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = sDummyCallbacks;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (outState != null && mBundle != null) {
			outState.putAll(mBundle);
		}
	}

//	@Override
//	public void onActivityCreated(Bundle savedInstanceState) {
//		super.onActivityCreated(savedInstanceState);
//		
//		mMapView.onCreate(savedInstanceState);
//		mMap = mMapView.getMap();
//		try {
//		    MapsInitializer.initialize(getActivity());
//		} catch (GooglePlayServicesNotAvailableException e1) {
//		    e1.printStackTrace();
//		}
//	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.grid_beer_item);
		mImageThumbSpacing = getResources().getDimensionPixelSize(R.dimen.grid_beer_item_space);

		mAdapter = new ImageAdapter(getActivity());
		
		((ActionBarActivity) getActivity()).setSupportProgressBarIndeterminateVisibility(true);		
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery(ParseConfig.OBJECT_BEER);
		query.setCachePolicy(CachePolicy.NETWORK_ELSE_CACHE);
		query.whereEqualTo(ParseConfig.POINTER_USER, ParseUser.getCurrentUser());
		query.findInBackground(new FindCallback<ParseObject>() {

			private List<ParseObject> mBeerList;

			@Override
			public void done(List<ParseObject> beerList, ParseException e) {

				if (getActivity() != null) {

					if (e != null) {
						Log.e(ParseConfig.TAG_ERROR, e.getMessage());
						return;
					} 
					
					if (beerList != null && beerList.size() > 0) {

						mBeerList = beerList;

						if (mBeerList != null && mBeerList.size() > 0) {
							mAdapter.setData(mBeerList);
							mAdapter.notifyDataSetChanged();
						}
						
						mGridView.setVisibility(View.VISIBLE);
//						mMapView.setVisibility(View.VISIBLE);
						mEmpty.setVisibility(View.GONE);
																		
//						int x = 1;
//						for (ParseObject beer : mBeerList) {
//							String postID = beer.getString(ParseConfig.BEER_POST_ID);
//							if (postID != null && postID.length() > 0) {
//								pointBeer(postID, x == mBeerList.size(), beer);
//							}
//							x++;
//						}
						
					} else {
						mGridView.setVisibility(View.GONE);
//						mMapView.setVisibility(View.GONE);
						mEmpty.setVisibility(View.VISIBLE);
					}										
				}
			}
		});				
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View v = inflater.inflate(R.layout.fragment_categories_grid, container, false);
				
		mEmpty = (View) v.findViewById(android.R.id.empty);	
		TextView mMsgIntro = (TextView) v.findViewById(R.id.msg_intro);		
		mMsgIntro.setTypeface(Fonts.getInstance().getRobotoCondensed(getActivity().getAssets()));
		
		TextView mMsgWelcome = (TextView) v.findViewById(R.id.welcome);		
		mMsgWelcome.setTypeface(Fonts.getInstance().getRobotoLight(getActivity().getAssets()));
		
		TextView mFirst = (TextView) v.findViewById(R.id.action_first);
		mFirst.setTypeface(Fonts.getInstance().getRobotoRegular(getActivity().getAssets()));
		mFirst.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				getActivity().startActivityForResult(new Intent(getActivity(), NewBeerActivity.class), MainActivity.REQUEST_CODE_NEW_BEER);
			}
		});
		
//		mMapView = (MapView) v.findViewById(R.id.map);
//		mMapView.onCreate(savedInstanceState);		
//		mMap = mMapView.getMap();
//		
//		try {
//			// so we can use the CameraUpdateFactory
//		    MapsInitializer.initialize(getActivity());
//		} catch (GooglePlayServicesNotAvailableException e) {
//		    e.printStackTrace();
//		}
		
		mGridView = (GridView) v.findViewById(R.id.grid);		
		mGridView.setAdapter(mAdapter);
		mGridView.setOnItemClickListener(this);
		mGridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				if (mAdapter.getNumColumns() == 0) {
					final int numColumns = (int) Math.floor(mGridView.getWidth() / (mImageThumbSize + mImageThumbSpacing));
					if (numColumns > 0) {
						final int columnWidth = (mGridView.getWidth() / numColumns) - mImageThumbSpacing;
						mAdapter.setNumColumns(numColumns);
						mAdapter.setItemHeight(columnWidth);
					}
				}
			}
		});
		
		return v;
	}

	@Override
	public void onResume() {
//		mMapView.onResume();
		super.onResume();
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@TargetApi(16)
	@Override
	public void onItemClick(AdapterView<?> parent, View v, final int position, final long id) {
		mCallbacks.choosed("", position);
	}
	
	public static class ViewHolder {
		public ImageView foto;		
	}

	private void pointBeer(String postID, final boolean last, final ParseObject beer) {
		
		ParseFBUtils.getInstance().getPost(getActivity(), postID, new CallbackFBRequest() {
 			
 			@Override
 			public void onCompleteFBRequest(Response response) { 			  	
 			  	
 				if (last) {
 					((ActionBarActivity) getActivity()).setSupportProgressBarIndeterminateVisibility(false);
 				}
 				
 			  	if (response == null) {
 			  		return;
 			  	}
 		  		
 			  	if (response.getGraphObject().getInnerJSONObject() == null) {
 		  			return;
 		  		}
 		  		
 		  		try {
 					JSONObject place = response.getGraphObject().getInnerJSONObject().getJSONObject("place");
					JSONObject location = place.getJSONObject("location");
 					
 					LatLng startLatLng = new LatLng(location.getLong("latitude"), location.getLong("longitude"));
 					
// 					mMap.addMarker(
// 						new MarkerOptions()
// 							.icon(BitmapDescriptorFactory.fromResource(R.drawable.point_beer))
// 							.position(startLatLng)
// 							.snippet(place.getString("name"))
// 							.title(beer.getString(ParseConfig.BEER_TITULO))
// 					);		
// 					
// 					LatLngBounds latLngBounds = new LatLngBounds(
//						new LatLng(startLatLng.latitude, startLatLng.longitude),
//						new LatLng(startLatLng.latitude, startLatLng.longitude)
//					);
//					mMap.moveCamera(
//						CameraUpdateFactory.newLatLngBounds(
//							latLngBounds,
//							getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin)
//						)
//					);
 					
 				} catch (JSONException e) {
 					Log.e("JSON Location", e.getMessage());
 				}			  	
 			}
 		});
	}
	
	/**
	 * The main adapter that backs the GridView. This is fairly standard except
	 * the number of columns in the GridView is used to create a fake top row of
	 * empty views as we use a transparent ActionBar and don't want the real top
	 * row of images to start off covered by it.
	 */
	private class ImageAdapter extends BaseAdapter {

		private final Context mContext;
		private int mItemHeight = 0;
		private int mNumColumns = 0;
		private GridView.LayoutParams mImageViewLayoutParams;
		
		private LayoutInflater inflater;
		private List<ParseObject> listData;		

		public ImageAdapter(Context context) {
			super();
			mContext = context;
			mImageViewLayoutParams = new GridView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			
			inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public void setData(List<ParseObject> mBeerList) {
			listData = mBeerList;
		}

		@Override
		public int getCount() {
			int count;
			if (listData == null) {
				count = 0;
			} else {
				count = listData.size();
			}
			return count;
		}

		@Override
		public ParseObject getItem(int position) {
			if (listData != null) {			
				return listData.get(position);
			}
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@SuppressLint("DefaultLocale")
		@Override
		public View getView(int position, View convertView, ViewGroup container) {

			ViewHolder viewHolder;
			if (convertView == null) {
				convertView =  inflater.inflate(R.layout.fragment_categories_grid_item, container, false);
				convertView.setLayoutParams(mImageViewLayoutParams);
				
				viewHolder = new ViewHolder();
				viewHolder.foto = (ImageView) convertView.findViewById(R.id.foto);				
				
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}

			// Check the height matches our calculated column width
			if (convertView.getLayoutParams().height != mItemHeight) {
				convertView.setLayoutParams(mImageViewLayoutParams);
			}

			final ParseObject beer = getItem(position);
			
			String foto = beer.getString(ParseConfig.BEER_FOTO);
			if (foto != null) {
				RequestCreator load = Picasso.with(mContext).load(foto);
				if (mItemHeight > 0) {					
					load.resize(mItemHeight, mItemHeight).centerCrop();
				}
				load.into(viewHolder.foto);
			}
			
			return convertView;
		}

		/**
		 * Sets the item height. Useful for when we know the column width so the
		 * height can be set to match.
		 * 
		 * @param height
		 */
		public void setItemHeight(int height) {
			if (height <= 0) {
				return;
			}
			if (height == mItemHeight) {
				return;
			}
			mItemHeight = height;
			mImageViewLayoutParams = new GridView.LayoutParams(LayoutParams.MATCH_PARENT, mItemHeight);
			notifyDataSetChanged();
		}

		public void setNumColumns(int numColumns) {
			mNumColumns = numColumns;
		}

		public int getNumColumns() {
			return mNumColumns;
		}

		@Override
		public int getItemViewType(int arg0) {
			return 1;
		}

		@Override
		public int getViewTypeCount() {
			return 1;
		}

		@Override
		public boolean isEnabled(int position) {
			return true;
		}
	}
}
