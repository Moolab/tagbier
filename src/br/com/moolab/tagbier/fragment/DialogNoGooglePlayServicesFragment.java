package br.com.moolab.tagbier.fragment;

import android.os.Bundle;
import br.com.moolab.tagbier.R;

public class DialogNoGooglePlayServicesFragment extends DialogJoyFragment {
	public static final String DIALOG_NO_PLAY_SERVICES = "DIALOG_NO_PLAY_SERVICES";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		icon = R.drawable.ic_beer;
		title = R.string.app_name;
		message = R.string.msg_no_connection;
		negativeLabel = R.string.action_later;
	}
}
