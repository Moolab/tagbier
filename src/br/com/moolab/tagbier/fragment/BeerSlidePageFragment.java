/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.moolab.tagbier.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import br.com.moolab.tagbier.MainActivity;
import br.com.moolab.tagbier.R;
import br.com.moolab.tagbier.interfaces.BeerCallback;
import br.com.moolab.tagbier.utils.Fonts;
import br.com.moolab.tagbier.utils.ParseConfig;
import br.com.moolab.tagbier.utils.Preferences;

import com.parse.CountCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQuery.CachePolicy;
import com.squareup.picasso.Picasso;

import de.passsy.holocircularprogressbar.HoloCircularProgressBar;

/**
 * A fragment representing a single step in a wizard. The fragment shows a dummy title indicating
 * the page number, along with some dummy text.
 *
 * <p>This class is used by the {@link CardFlipActivity} and {@link
 * MainActivity} samples.</p>
 */
public class BeerSlidePageFragment extends Fragment {
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int mPageNumber;	

	private Typeface robotoCondensed;

	private Typeface robotoLight;

	private Typeface robotoThin;

	private Typeface robotoNormal;

	private ViewHolder holder;

	private int tagAlpha = 255;

	private BeerCallback mCallbacks = sDummyCallbacks;

	private static BeerCallback sDummyCallbacks = new BeerCallback() {

		@Override
		public void showPlaces(String objectID) {
		
		}
	};

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (!(activity instanceof BeerCallback)) {
			throw new IllegalStateException("Activity must implement fragment's callbacks.");
		}

		mCallbacks = (BeerCallback) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = sDummyCallbacks;
	}
	
    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     * @param parseObject 
     * @throws JSONException 
     */
    public static BeerSlidePageFragment create(int pageNumber, ParseObject beer) throws JSONException {
        BeerSlidePageFragment fragment = new BeerSlidePageFragment();
		
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        args.putString(ParseConfig.BEER_FOTO, beer.getString(ParseConfig.BEER_FOTO));
        args.putString(ParseConfig.OBJCETID, beer.getObjectId());
        args.putString(ParseConfig.BEER_TITULO, beer.getString(ParseConfig.BEER_TITULO));
        args.putString(ParseConfig.BEER_TIPO, beer.getString(ParseConfig.BEER_TIPO));
        
        ArrayList<String> tags = new ArrayList<String>();     
        JSONArray jsonArray = beer.getJSONArray(ParseConfig.BEER_HASHTAGS); 
        if (jsonArray != null) { 
           int len = jsonArray.length();
           for (int i=0;i<len;i++){ 
        	   tags.add(jsonArray.get(i).toString());
           } 
        } 
        args.putStringArrayList(ParseConfig.BEER_HASHTAGS, tags);
    	
        Date quando = beer.getDate(ParseConfig.BEER_QUANDO);
    	Calendar cQuando = (Calendar) Calendar.getInstance().clone();
    	cQuando.setTimeZone(TimeZone.getDefault());
    	cQuando.setTime(quando);
    	if (cQuando != null) {
    		args.putLong(ParseConfig.BEER_QUANDO, cQuando.getTimeInMillis());
    	}
        
        fragment.setArguments(args);
        
        return fragment;
    }

    public BeerSlidePageFragment() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);        
        mPageNumber = getArguments().getInt(ARG_PAGE);                
		
		String foto = getArguments().getString(ParseConfig.BEER_FOTO);
		if (foto != null) {
			Picasso.with(getActivity()).load(foto).resizeDimen(R.dimen.grid_beer_item_width, R.dimen.grid_beer_item_width).centerCrop().into(holder.foto);
		}
		
		String titulo = getArguments().getString(ParseConfig.BEER_TITULO);
		holder.titulo.setText(titulo);
		
		String tipo = getArguments().getString(ParseConfig.BEER_TIPO);
		holder.tipo.setText(tipo);
		
		holder.onde.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCallbacks.showPlaces(getArguments().getString(ParseConfig.OBJCETID));
			}
		});
		
		ArrayList<String> tagsList = getArguments().getStringArrayList(ParseConfig.BEER_HASHTAGS);
		loadHashTags(holder.tags, tagsList);
		
		ParseQuery<ParseObject> queryCountAgree = ParseQuery.getQuery(ParseConfig.OBJECT_BEER);
		queryCountAgree.setCachePolicy(CachePolicy.CACHE_ELSE_NETWORK);
		queryCountAgree.whereEqualTo(ParseConfig.BEER_TITULO, titulo);
		queryCountAgree.whereEqualTo(ParseConfig.BEER_TIPO, tipo);
		queryCountAgree.countInBackground(new CountCallback() {
			
			@Override
			public void done(int count, ParseException e) {
				if (e != null) {
					Log.e(ParseConfig.TAG_ERROR, e.getMessage());
				} else {
					if (getActivity() != null) {
						float percentDrunk = (count * 100) / Preferences.getTotalUser(getActivity());
						animate(holder.graficDrunk, null, percentDrunk / 100, 2000);
						holder.drunk.setText(String.valueOf(Math.round(percentDrunk)) + "%");
						holder.loadingDrunk.setVisibility(View.GONE);
					}
				}
			}
		});
		
		Calendar cNow = Calendar.getInstance();
		
		Calendar cQuando = (Calendar) Calendar.getInstance().clone();
		cQuando.setTimeInMillis(getArguments().getLong(ParseConfig.BEER_QUANDO));
		
		long milliseconds1 = cQuando.getTimeInMillis();
	    long milliseconds2 = cNow.getTimeInMillis();
	    long diff = milliseconds2 - milliseconds1;
//	    long diffSeconds = diff / 1000;
//	    long diffMinutes = diff / (60 * 1000);
//	    long diffHours = diff / (60 * 60 * 1000);
	    long diffDays = diff / (24 * 60 * 60 * 1000);
	    
	    String quando = getActivity().getString(R.string.since) + " ";
	    if (diffDays > 1) {
	    	quando += diffDays + " " + getActivity().getString(R.string.quando_days);
	    } else if (diffDays == 1) {
	    	quando = getActivity().getString(R.string.yesterday);
	    } else if (diffDays == 0) {
	    	quando = getActivity().getString(R.string.today);
	    }
		
		holder.quando.setText(quando);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup convertView = (ViewGroup) inflater.inflate(R.layout.page_beer, container, false);

        robotoCondensed = Fonts.getInstance().getRobotoCondensed(getActivity().getAssets());
		robotoNormal = Fonts.getInstance().getRobotoRegular(getActivity().getAssets());
		robotoLight = Fonts.getInstance().getRobotoLight(getActivity().getAssets());
		robotoThin = Fonts.getInstance().getRobotoThin(getActivity().getAssets());
        
		holder = new ViewHolder();
		
		holder.titulo = (TextView) convertView.findViewById(R.id.titulo);
		holder.titulo.setTypeface(robotoCondensed);
		
//		holder.agreeDesc = (TextView) convertView.findViewById(R.id.agree_desc);
//		holder.agreeDesc.setTypeface(robotoNormal);

//		holder.agree = (TextView) convertView.findViewById(R.id.agree_value);
//		holder.agree.setTypeface(robotoCondensed);
		
		holder.drunkDesc = (TextView) convertView.findViewById(R.id.drunk_desc);
		holder.drunkDesc.setTypeface(robotoNormal);
		
		holder.drunk = (TextView) convertView.findViewById(R.id.drunk_value);
		holder.drunk.setTypeface(robotoCondensed);
		
		holder.tipo = (TextView) convertView.findViewById(R.id.tipo);
		holder.tipo.setTypeface(robotoLight);
		
		holder.quando = (TextView) convertView.findViewById(R.id.last_time);
		holder.quando.setTypeface(robotoCondensed);

		holder.onde = (Button) convertView.findViewById(R.id.last_places);
		holder.onde.setTypeface(robotoThin);
		
		holder.tags = (LinearLayout) convertView.findViewById(R.id.tags);
		
		holder.foto = (ImageView) convertView.findViewById(R.id.foto);
		
		holder.loadingDrunk = (ProgressBar) convertView.findViewById(R.id.loading_drunk);
//		holder.loadingAgree = (ProgressBar) convertView.findViewById(R.id.loading_agree);
		
//		holder.graficAgree = (HoloCircularProgressBar) convertView.findViewById(R.id.agree);
		holder.graficDrunk = (HoloCircularProgressBar) convertView.findViewById(R.id.drunk);				
        
        return convertView;
    }
    
    private void animate(final HoloCircularProgressBar progressBar,final AnimatorListener listener, final float progress, final int duration) {

		ObjectAnimator mProgressBarAnimator = ObjectAnimator.ofFloat(progressBar, "progress", progress);
        mProgressBarAnimator.setDuration(duration);

        mProgressBarAnimator.addListener(new AnimatorListener() {

                @Override
                public void onAnimationCancel(final Animator animation) {
                }

                @Override
                public void onAnimationEnd(final Animator animation) {
                    progressBar.setProgress(progress);
                }

                @Override
                public void onAnimationRepeat(final Animator animation) {
                }

                @Override
                public void onAnimationStart(final Animator animation) {
                }
        });
        if (listener != null) {
            mProgressBarAnimator.addListener(listener);
        }
        mProgressBarAnimator.reverse();
        mProgressBarAnimator.addUpdateListener(new AnimatorUpdateListener() {

                @Override
                public void onAnimationUpdate(final ValueAnimator animation) {
                    progressBar.setProgress((Float) animation.getAnimatedValue());
                }
        });
        progressBar.setMarkerProgress(progress);
        mProgressBarAnimator.start();
	}
	
	public static class ViewHolder {		
		public ProgressBar loadingAgree;
		public ProgressBar loadingDrunk;
		public HoloCircularProgressBar graficAgree;
		public HoloCircularProgressBar graficDrunk;
		
		public TextView drunkDesc;
		public TextView agreeDesc;
		
		public TextView drunk;
		public TextView agree;
		
		public LinearLayout tags;
		public TextView titulo;
		public TextView tipo;
		public TextView quando;
		public Button onde;
		public ImageView foto;
	}
	
	private void loadHashTags(LinearLayout tags, ArrayList<String> arrayList) {
		tags.removeAllViews();
		if (arrayList.size() == 0) {
			tags.setVisibility(View.GONE);
		}
		for (int i = 0; i < arrayList.size(); i++) {
			tags.addView(getHashTagView(tags, arrayList.get(i)));
		}
	}

	private View getHashTagView(LinearLayout tags, String tag) {
		TextView child = (TextView) getActivity().getLayoutInflater().inflate(R.layout.hashtag, tags, false);
		child.setTypeface(robotoCondensed);
		child.setText(tag);
		child.setBackgroundColor(Color.argb(tagAlpha, 0, 154, 140));
		tagAlpha -= 60;
		return child;
	}

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
}
