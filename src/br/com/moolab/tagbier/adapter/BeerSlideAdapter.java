package br.com.moolab.tagbier.adapter;

import java.util.List;

import org.json.JSONException;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import br.com.moolab.tagbier.fragment.BeerSlidePageFragment;

import com.parse.ParseObject;

public class BeerSlideAdapter extends FragmentStatePagerAdapter {
	    
	private static final String TAG_ERROR = "ERRO_BEER_SLIDE_ADAPTER";
	private List<ParseObject> mBeerList;

	public BeerSlideAdapter(FragmentManager fm) {    	
        super(fm);
    }	

    @Override
    public Fragment getItem(int position) {
        BeerSlidePageFragment create = null;
		try {
			create = BeerSlidePageFragment.create(position, mBeerList.get(position));
		} catch (JSONException e) {
			Log.e(TAG_ERROR, e.getMessage());
		}
		return create;
    }

    @Override
    public int getCount() {
        return mBeerList.size();
    }

	public void setData(List<ParseObject> beerList) {
		mBeerList = beerList;
	}
}