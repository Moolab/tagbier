package br.com.moolab.tagbier.adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.moolab.tagbier.R;
import br.com.moolab.tagbier.utils.Fonts;

import com.squareup.picasso.Picasso;

public class NavigationDrawerAdapter extends BaseAdapter {

	public static final String THUMB = "thumb";
	public static final String TITLE = "title";
	private LayoutInflater mInflater;
	private JSONArray listData;
	private Context mContext;

	public NavigationDrawerAdapter(Context context) {
		super();
		mContext  = context;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setData(JSONArray tipsList) {
		listData = tipsList;
	}

	@Override
	public int getCount() {
		int count;
		if (listData == null) {
			count = 0;
		} else {
			count = listData.length();
		}
		return count;
	}

	@Override
	public JSONObject getItem(int position) {
		if (listData != null) {
			try {
				return listData.getJSONObject(position);
			} catch (JSONException e) {
				Log.e("Erro JSON", e.getMessage());
			}
		}
		return null;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup container) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.drawer_list_item, container, false);
			holder.thumb = (ImageView) convertView.findViewById(R.id.thumb);
			holder.title = (TextView) convertView.findViewById(R.id.title);
			holder.title.setTypeface(Fonts.getInstance().getRobotoThin(mContext.getAssets()));
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		JSONObject item = getItem(position);
		
		try {
			int thumb = item.getInt(THUMB);
			if (thumb > 0) {
				Picasso.with(mContext).load(thumb)
					.resizeDimen(R.dimen.navigation_thumb, R.dimen.navigation_thumb).centerInside()
					.placeholder(R.drawable.empty_photo).into(holder.thumb);				
			} 
			
		} catch (JSONException e) {
			Log.e("Erro JSON", e.getMessage());
		}		

		try {
			holder.title.setText(item.getString(TITLE));
		} catch (JSONException e) {
			Log.e("Erro JSON", e.getMessage());
		}

		return convertView;
	}

	@Override
	public int getItemViewType(int arg0) {
		return 1;
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	public static class ViewHolder {
		public ImageView thumb;
		public TextView title;
	}
}
